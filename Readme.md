# Technical Test Signaturit

This project is technical test for Signaturit by *Francisco Manuel Ortiz Briegas*.

## Technologies

Proyect developed with XAMPP, Symfony 4.4, Visual Studio Code as IDE on Windows.

## Usage

```
# First Phase Command
php bin/console app:test:first-phase --help

# Second Phase Command
php bin/console app:test:second-phase --help

# Testing Service
php vendor/bin/phpunit --filter SignaturitServiceTest
```