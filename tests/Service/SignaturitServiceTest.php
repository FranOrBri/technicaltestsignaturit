<?php

namespace App\Tests\Service;

use App\Service\SignaturitService;
use PHPUnit\Framework\TestCase;

class SignaturitServiceTest extends TestCase
{
    const INVALID_CONTRACT = "N##";
    const WINNER_CONTRACT = "KN";
    const LOOSER_CONTRACT = "NNV";

    const INCOMPLETE_CONTRACT = "N#V";
    const ALWAYS_WIN_CONTRACT = "V";
    const KING_WIN_CONTRACT = "KV";
    const NOTARY_WIN_CONTRACT = "NVV";
    const VALIDATOR_WIN_CONTRACT = "NV";
    const NO_WIN_CONTRACT = "KKK";

    /**
     * @var SignaturitService
     */
    protected SignaturitService $signaturitService;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->signaturitService = new SignaturitService();
    }

    /**
     * @return void
     */
    public function testCalculateWinnerDrawResult()
    {
        $winner = $this->signaturitService->calculateWinner($this::WINNER_CONTRACT, $this::WINNER_CONTRACT);
        $this->assertTrue($winner === $this->signaturitService::DRAW_RESULT);
    }

    /**
     * @return void
     */
    public function testCalculateWinnerPlaintiffWinner()
    {
        $winner = $this->signaturitService->calculateWinner($this::WINNER_CONTRACT, $this::LOOSER_CONTRACT);
        $this->assertTrue($winner === $this->signaturitService::PLAINTIFF_RESULT);
    }

    /**
     * @return void
     */
    public function testCalculateWinnerDefendantWinner()
    {
        $winner = $this->signaturitService->calculateWinner($this::LOOSER_CONTRACT, $this::WINNER_CONTRACT);
        $this->assertTrue($winner === $this->signaturitService::DEFENDANT_RESULT);
    }

    /**
     * @return void
     */
    public function testCalculateWinnerInvalidResult()
    {
        $winner = $this->signaturitService->calculateWinner($this::WINNER_CONTRACT, $this::INVALID_CONTRACT);
        $this->assertTrue($winner === $this->signaturitService::INVALID_RESULT);
    }

    /**
     * @return void
     */
    public function testCalculateSignatureToWinAlwaysResult()
    {
        $winner = $this->signaturitService->calculateSignatureToWin($this::INCOMPLETE_CONTRACT, $this::ALWAYS_WIN_CONTRACT);
        $this->assertTrue($winner === $this->signaturitService::WIN_RESULT);
    }

    /**
     * @return void
     */
    public function testCalculateSignatureToWinKingResult()
    {
        $winner = $this->signaturitService->calculateSignatureToWin($this::INCOMPLETE_CONTRACT, $this::KING_WIN_CONTRACT);
        $this->assertTrue($winner === $this->signaturitService::KING_RESULT);
    }

    /**
     * @return void
     */
    public function testCalculateSignatureToWinNotaryResult()
    {
        $winner = $this->signaturitService->calculateSignatureToWin($this::INCOMPLETE_CONTRACT, $this::NOTARY_WIN_CONTRACT);
        $this->assertTrue($winner === $this->signaturitService::NOTARY_RESULT);
    }

    /**
     * @return void
     */
    public function testCalculateSignatureToWinValidatorResult()
    {
        $winner = $this->signaturitService->calculateSignatureToWin($this::INCOMPLETE_CONTRACT, $this::VALIDATOR_WIN_CONTRACT);
        $this->assertTrue($winner === $this->signaturitService::VALIDATOR_RESULT);
    }

    /**
     * @return void
     */
    public function testCalculateSignatureToWinLooseResult()
    {
        $winner = $this->signaturitService->calculateSignatureToWin($this::INCOMPLETE_CONTRACT, $this::NO_WIN_CONTRACT);
        $this->assertTrue($winner === $this->signaturitService::LOOSE_RESULT);
    }

    /**
     * @return void
     */
    public function testCalculateSignatureInvalidResult()
    {
        $winner = $this->signaturitService->calculateSignatureToWin($this::INCOMPLETE_CONTRACT, $this::INCOMPLETE_CONTRACT);
        $this->assertTrue($winner === $this->signaturitService::INVALID_RESULT);
    }
}