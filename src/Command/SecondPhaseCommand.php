<?php

namespace App\Command;

use App\Service\SignaturitService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SecondPhaseCommand extends Command
{
    const SUCCESS = 0;
    const INVALID = 2;

    /**
     * @var string
     */
    protected static $defaultName = 'app:test:second-phase';

    /**
     * @var SignaturitService
     */
    private SignaturitService $signaturitService;

    /**
     * @param SignaturitService $signaturitService
     */
    public function __construct(SignaturitService $signaturitService)
    {
        $this->signaturitService = $signaturitService;
        parent::__construct();
    }

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this->setDescription('Test second Phase given both contracts signatures where one signature per part can be empty.')
            ->setHelp('This command prints the signature required to win the trial.')
            ->addArgument('plaintiffSignatures', InputArgument::OPTIONAL, 'Signatures of the plaintiff\'s contract.')
            ->addArgument('defendantSignatures', InputArgument::OPTIONAL, 'Signatures of the defendant\'s contract.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $plaintiffSignatures = $input->getArgument('plaintiffSignatures');
        $defendantSignatures = $input->getArgument('defendantSignatures');

        if (!$plaintiffSignatures || !$defendantSignatures) {
            $output->writeln([
                '<comment>Usage</comment>',
                '  app:test:second-phase <plaintiffSignatures> <defendantSignatures>'
            ]);
            return $this::INVALID;
        }

        $output->writeln([
            'Second Phase Test',
            '============',
            '',
            'Plaintiff\'s signatures: ' . $plaintiffSignatures,
            'Defendant\'s signatures: ' . $defendantSignatures,
            '',
        ]);

        $result = $this->signaturitService->calculateSignatureToWin($plaintiffSignatures, $defendantSignatures);

        $output->write('<info>' . $result . '</info>');

        return $this::SUCCESS;
    }
}
