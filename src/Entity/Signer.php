<?php

namespace App\Entity;

class Signer
{
    /**
     * @var string
     */
    private string $role;

    /**
     * @param string $role
     */
    public function __construct(string $role)
    {
        $this->role = $role;
    }

    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * @param string $role
     * @return void
     */
    public function setRole(string $role): void
    {
        $this->role = $role;
    }

    /**
     * @return int
     */
    public function getPoints(): int
    {
        switch (strtoupper($this->getRole())) {
            case 'K':
                $points = 5;
                break;
            case 'N':
                $points = 2;
                break;
            case 'V':
                $points = 1;
                break;
            default:
                $points = 0;
                break;
        }

        return $points;
    }
}
