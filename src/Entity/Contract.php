<?php

namespace App\Entity;

class Contract
{
    /**
     * @var array
     */
    private array $signers;

    /**
     * @param array $signers
     */
    public function __construct(array $signers)
    {
        $this->signers = $signers;
    }

    /**
     * @return array
     */
    public function getSigners(): array
    {
        return $this->signers;
    }

    /**
     * @param array $signers
     * @return void
     */
    public function setSigners(array $signers): void
    {
        $this->signers = $signers;
    }

    /**
     * @return int
     */
    public function calculatePoints(): int
    {
        $points = 0;
        foreach ($this->signers as $signer) {
            $points += $signer->getPoints();
        }
        
        return $points;
    }

    /**
     * @return int
     */
    public function checkInvalidSigners(): int
    {
        $invalidSigners = 0;
        foreach ($this->signers as $signer) {
            switch (strtoupper($signer->getRole())) {
                case 'K':
                case 'N':
                case 'V':
                    break;
                default:
                    $invalidSigners++;
                    break;
            }
        }
        
        return $invalidSigners;
    }
}
