<?php

namespace App\Service;

use App\Entity\Contract;
use App\Entity\Signer;

class SignaturitService
{
    const INVALID_RESULT = 'Some contracts\'s signatures are wrong';

    const DRAW_RESULT = 'There is a draw!';
    const PLAINTIFF_RESULT = 'Plaintiff wins!';
    const DEFENDANT_RESULT = 'Defendant wins!';

    const WIN_RESULT = 'You have already won this trial';
    const KING_RESULT = 'You need a king\'s signature';
    const NOTARY_RESULT = 'You need a notary\'s signature';
    const VALIDATOR_RESULT = 'You need a validator\'s signature';
    const LOOSE_RESULT = 'There is no chance to win this trial';

    /**
     * @param string $plaintiffContract
     * @param string $defendantContract
     * @return string
     */
    public function calculateWinner(string $plaintiffContract, string $defendantContract): string
    {
        $plaintiffContract = $this->generateContract($plaintiffContract);
        $defendantContract = $this->generateContract($defendantContract);

        if ($plaintiffContract->checkInvalidSigners() || $defendantContract->checkInvalidSigners()) {
            $result = $this::INVALID_RESULT;
        } else {
            $result = $this->compareContracts($plaintiffContract, $defendantContract);
        }

        return $result;
    }

    /**
     * @param string $plaintiffContract
     * @param string $defendantContract
     * @return string
     */
    public function calculateSignatureToWin(string $plaintiffContract, string $defendantContract): string
    {
        $plaintiffContract = $this->generateContract($plaintiffContract);
        $defendantContract = $this->generateContract($defendantContract);

        $plaintiffPoints = $plaintiffContract->calculatePoints();
        $defendantPoints = $defendantContract->calculatePoints();

        if ($plaintiffContract->checkInvalidSigners() > 1 || $defendantContract->checkInvalidSigners() > 1 || $plaintiffContract->checkInvalidSigners() + $defendantContract->checkInvalidSigners() > 1) {
            $result = $this::INVALID_RESULT;
        } else {
            $result = $this->requiredSignature($plaintiffPoints, $defendantPoints);
        }

        return $result;
    }

    /**
     * @param string $contractSigners
     * @return Contract
     */
    private function generateContract(string $contractSigners): Contract
    {
        // Extract each character from the contracts
        $tokens = str_split($contractSigners);

        // Set a Signer from each character
        $signers = [];
        foreach ($tokens as $role) {
            $signers[] = new Signer($role);
        }

        // Set a Contract with their respective signers
        $contract = new Contract($signers);

        return $contract;
    }

    /**
     * @param Contract $plaintiffContract
     * @param Contract $defendantContract
     * @return string
     */
    private function compareContracts(Contract $plaintiffContract, Contract $defendantContract): string
    {
        // Get the score of each Signer
        $plaintiffPoints = $plaintiffContract->calculatePoints();
        $defendantPoints = $defendantContract->calculatePoints();
        $winner = $this::DRAW_RESULT;

        // Check scores and calculate winner
        if ($plaintiffPoints > $defendantPoints) {
            $winner = $this::PLAINTIFF_RESULT;
        } else if ($plaintiffPoints < $defendantPoints) {
            $winner = $this::DEFENDANT_RESULT;
        }

        return $winner;
    }

    /**
     * @param int $plaintiffPoints
     * @param int $defendantPoints
     * @return string
     */
    private function requiredSignature(int $plaintiffPoints, int $defendantPoints): string
    {
        // Calculate the signature required to win if a Signer is missing
        $absDifference = abs($plaintiffPoints - $defendantPoints);

            if ($plaintiffPoints > $defendantPoints) {
                $result = $this::WIN_RESULT;
            } else if ($absDifference < 1) {
                $result = $this::VALIDATOR_RESULT;
            } else if ($absDifference < 2) {
                $result = $this::NOTARY_RESULT;
            } else if ($absDifference < 5) {
                $result = $this::KING_RESULT;
            } else {
                $result = $this::LOOSE_RESULT;
            }

            return $result;
    }
}
